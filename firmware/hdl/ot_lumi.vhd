-- IEEE VHDL standard library:
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-- Library work
library work;
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.ipbus_decode_ot_lumi.all;

-- bril package
use work.bril_histogram_package.all;

-- This is the LHC structure
--
--   (0)(1).(DEB)(0)(1).(DEB)...(0)(1).(DEB)(0)(1).(DEB)(0)(1).(DEB)...(0)(1).(DEB)...(0)(1).(DEB)(0)(1).(DEB)...(0)(1).(DEB)(0)(1)...
--   [    0     ][     1    ]...[   4095   ][   4096   ][   4097   ]...[   8191   ]...[  258048  ][  258049  ]...[  262143  ][  262144  ]...
--   {            Nibble  1                }{            Nibble  2                }...{            Nibble 64                }{            Nibble 65                }...
--   <                                                 Lumi Section  1                                                      ><          Lumi Section  2
--  ^                                      ^                                      ^  ^                                      ^
-- BGo0                                   BGo0                                 BGo0  BGo0                                  BGo0
-- OC0 
-- Start
-- BC0 every 3564 BX
--
-- (1 BX)                           =   25 ns (40    MHz),  counter 12 bits,  0-3563 (N/A @BRIL module)
-- [1 Orbit]          = 3564 BX     = 89.1 us (11.22 kHz),  counter 32 bits   0-...  (N/A @BRIL module)
-- {1 Nibble}         = 4096 orbits =  365 ms (2.74   Hz),  counter  6 bits   1-64   (32b @BRIL module)
-- <1 Lumi Section>   = 64 Nibbles  = 23.3 s  (42    mHz),  counter 12 bits   1-...  (32b @BRIL module)


entity ot_lumi is
  port(
    -- ipbus
    ipb_clk             : in std_logic;
    ipb_rst             : in std_logic;
    ipb_in              : in ipb_wbus;
    ipb_out             : out ipb_rbus;

    -- ttc
    --bc0_signal_i        : in std_logic;

    -- stub
    clk                 : in  std_logic;
    rst                 : in  std_logic
    --bc0_signal          : inout std_logic;
    --bg0_signal          : inout std_logic
  );
end entity;

architecture Behavioural of ot_lumi is

  -- IPBUS BRANCHES
  signal ipb_to_slaves  : ipb_wbus_array(N_SLAVES - 1 downto 0);
  signal ipb_from_slaves: ipb_rbus_array(N_SLAVES - 1 downto 0);
  -- IPBUS REGISTER
  constant N_STAT : integer := 4;
  constant N_CTRL : integer := 4;
  signal ctrl_stb: std_logic_vector(N_CTRL-1 downto 0);
  signal stat: ipb_reg_v(N_STAT-1 downto 0);
  signal conf: ipb_reg_v(N_CTRL-1 downto 0);

  -- TODO: Check later
  signal lumi_section           : std_logic_vector(31 downto 0);
  signal lumi_nibble            : std_logic_vector(31 downto 0);
  signal new_histogram          : std_logic;
  signal bc0_signal             : std_logic;
  signal bg0_signal             : std_logic; 
  signal bril_data              : std_logic_vector(31 downto 0);
  signal bril_fifo_rd_en        : std_logic; 
  signal bril_fifo_empty        : std_logic; 
  signal bril_fifo_words_cnt    : std_logic_vector(31 downto 0); 
  signal bril_fifo_data_valid   : std_logic;
  signal user_reset             : std_logic;
 
  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals declaration
  signal conf_reset_nibble_reg, conf_reset_nibble_ttc : std_logic := '0';

  signal conf_new_histo_nibble_mask : std_logic_vector(3 downto 0) := (others => '0');
  signal conf_bgo_value : std_logic_vector(7 downto 0) := (others => '0');
  signal conf_data_latency : std_logic_vector(15 downto 0) := (others => '0');
  signal conf_fill_number, conf_run_number, stat_lumi_nibble, stat_lumi_section : std_logic_vector(31 downto 0) := (others => '0');

  -- End of automatically-generated VHDL code for register "breakout" signals declaration
  --------------------------------------------------------------------------------


begin

  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals assignment

  --conf_run_number            <= conf(16#00#)(31 downto 0);
  --conf_fill_number           <= conf(16#01#)(31 downto 0);
  --conf_bgo_value             <= conf(16#02#)(7 downto 0);
  --conf_new_histo_nibble_mask <= conf(16#02#)(11 downto 8);
  --conf_reset_nibble_reg      <= conf(16#02#)(12);
  --conf_reset_nibble_ttc      <= conf(16#02#)(13);
  --conf_data_latency          <= conf(16#02#)(31 downto 16);
  --stat_lumi_section          <= conf(16#03#)(31 downto 0);
  --stat_lumi_nibble           <= conf(16#04#)(31 downto 0);
  
  --stat(16#00#)(31 downto 0)  <= stat_lumi_section;
  --stat(16#01#)(31 downto 0)  <= stat_lumi_nibble;

  -- End of automatically-generated VHDL code for register "breakout" signals assignment
  --------------------------------------------------------------------------------

  --------------------------------------
  --------------------------------------
  -- IPbus
  --------------------------------------
  --------------------------------------
  
  -- ipbus address decode
  fabric: entity work.ipbus_fabric_sel
    generic map(
      NSLV => N_SLAVES,
      SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map(
      ipb_in => ipb_in,
      ipb_out => ipb_out,
      sel => ipbus_sel_ot_lumi(ipb_in.ipb_addr),
      ipb_to_slaves => ipb_to_slaves,
      ipb_from_slaves => ipb_from_slaves
    );
  
  sync_reg: entity work.ipbus_syncreg_v
  generic map(N_CTRL => N_CTRL, N_STAT => N_STAT)
  port map(
    clk => ipb_clk,
    rst => ipb_rst,
    ipb_in => ipb_to_slaves(N_SLV_OT_LUMI_REGISTERS),
    ipb_out => ipb_from_slaves(N_SLV_OT_LUMI_REGISTERS),
    slv_clk => clk,
    d => stat,
    q => conf,
    --rstb => stat_stb,
    stb => open
  );

  --------------------------------------
  --------------------------------------
  -- TTC DECODER
  --------------------------------------
  --------------------------------------
  bc0_strobe: process(clk)
    --==============================--
    
    variable timer : natural range 0 to 3563 := 3563;
  
  begin
      
      if rising_edge(clk) then
          
          if timer = 0 then
              timer := 3563;
              bc0_signal <= '1';
          else
              timer := timer - 1;
              bc0_signal <= '0';
          end if;
              
      end if;
  end process bc0_strobe;

  -- possible to create fake BG0 adding counter for 4096 orbits
  bg0_strobe: process(clk)
    variable timer : natural range 0 to 4095 := 4095;
  begin
    if bc0_signal = '1' then
    
      if timer = 0 then
        timer := 4095;
        bg0_signal <= '1';
      else
        timer := timer - 1;
        bg0_signal <= '0';
      end if;
    end if;
  end process bg0_strobe;

  new_histogram <= bg0_signal; --bg0_signal when conf_new_histo_nibble_mask(to_integer(unsigned(lumi_nibble(1 downto 0)))) = '0' else '0';

  ttc_decoder_inst: entity work.ttc_lumi_decoder
  port map(
    -- TTC inputs
    clk                 => clk                      ,
    rst                 => rst                      ,
    bc0_signal_i        => bc0_signal               ,
    bg0_signal_i        => bg0_signal               ,
    -- Config
    bgo_value           => conf_bgo_value           ,
    reset_nibble_reg    => conf_reset_nibble_reg    ,
    reset_nibble_ttc    => conf_reset_nibble_ttc    ,
    -- Outputs
    --bc0_signal          => bc0_signal               ,   
    --bg0_signal          => bg0_signal               ,
    lumi_nibble_o       => lumi_nibble              ,  
    lumi_section_o      => lumi_section   
  );
  
  stat_lumi_section <= lumi_section;
  stat_lumi_nibble <= lumi_nibble;

  --------------------------------------
  --------------------------------------
  -- BRIL MODULES
  --------------------------------------
  --------------------------------------

  bril_histogram_top_sync_inst: entity work.bril_histogram_top_sync
	generic map(
    -- mux ratio (defines data period, can be also clock ratio)
    MUX_RATIO                               => 1,
    -- identification of the histogram
    HISTOGRAM_TYPE                          => 0,
    HISTOGRAM_ID                            => 1,
    -- memory
    GENERATE_FIFO                           => true,
    -- histogram parameters
    NUMBER_OF_BINS_PER_CLK                  => 1, -- defines how many bins are transferred every clk_data_i, has to be N*MUX_RATIO, N >= 1
    NUMBER_OF_BINS_TOTAL                    => 3564,
    COUNTER_WIDTH                           => 16,
    INCREMENT_WIDTH                         => 1,
    -- orbit and bx counter width
    ORBIT_BX_COUNTER_WIDTH                  => 16,
    -- accumulation parameters
    NUMBER_OF_UNITS                         => 1, -- number of units (modules, chips) connected to the histogrammer
    UNIT_INCREMENT_WIDTH                    => 1, -- width of the data bus per ech unit
    NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE  => 4, -- defines the complexity of the accumualtion algorithm
    UNIT_ERROR_COUNTER_WIDTH                => 3, -- width of the error counter    
    CHECK_ERRORS                            => false,
    STORE_ERRORS                            => false
  )
  port map(
    -- core signals
    async_reset_i       => user_reset, -- sync reset
    clk_data_i          => clk, -- data clock, at which all inputs (increments, new histogram/iteration, tcds) arrive, not more than 320MHz
    clk_data_en_i       => '1', -- clock enable signal (can be used when clk_data_i is same as clk_histogramming_i but MUX_RATIO is more)
    clk_histogramming_i => clk, -- histogramming clock, allows to spare fpga resources by processing multiple bins in parallel, not more than 320MHz
    -- tcds data
    lhc_fill_i          => conf_fill_number,
    cms_run_i           => conf_run_number,
    lumi_section_i      => lumi_section,
    lumi_nibble_i       => lumi_nibble,
    -- SYNC MODE - tcds signals
    data_latency_i      => conf_data_latency,
    new_histogram_i     => new_histogram, -- start outputting data (orbit reset)
    new_iteration_i     => bc0_signal, -- resetting the bin counter (if number of bins is 3564 corresponding to 3564bx - this signal is the bx reset)
    -- data input interface (used both in sync and async mode)
    increment_i(0)      => '1',
    error_i             => (others => '0'),
    mask_i              => (others => '0'),
    -- fifo interface (optional)
    fifo_rd_clk_i       => ipb_clk,
    fifo_rd_en_i        => bril_fifo_rd_en,
    fifo_empty_o        => bril_fifo_empty,
    fifo_words_cnt_o    => bril_fifo_words_cnt,
    --
    data_o              => bril_data, -- output of the memory
    data_valid_o        => bril_fifo_data_valid -- write enable
  );

  readout_regs_inst: entity work.bril_histogram_readout_ipbus
  port map(
    clk                   => ipb_clk, -- ipbus clock                            
    reset                 => ipb_rst, -- reset in the ipbus clock domain                 
    ipbus_in              => ipb_to_slaves(N_SLV_BRIL),  -- ipbus input (SLV_BRIL_HISTOGRAM_READOUT is the slave id)
    ipbus_out             => ipb_from_slaves(N_SLV_BRIL),  -- ipbus output (SLV_BRIL_HISTOGRAM_READOUT is the slave id)
    -- fifo status
    fifo_empty_i          => bril_fifo_empty, -- connect to the respective bril histogram port
    fifo_data_valid_i     => bril_fifo_data_valid, -- connect to the respective bril histogram port
    fifo_words_cnt_i      => bril_fifo_words_cnt, -- connect to the respective bril histogram port
    -- fifo read interface
    fifo_rd_en_o          => bril_fifo_rd_en, -- connect to the respective bril histogram port
    fifo_data_i           => bril_data, -- connect to the respective bril histogram port
    -- user reset
    user_reset_o          => user_reset
  );
  
  
end architecture;
