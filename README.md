# OT lumi histogramming

## Implementation in OT DTC

To add this module to OT DTC, clone the repository:
`ipbb add git ssh://git@gitlab.cern.ch:7999/tselezne/ot-lumi-histogramming.git`

In /working_dir/src/dtc/top/dtc-mini/common/firmware/cfg/payload.dep, add the following dependency:
`include -c ot-lumi-histogramming:common bril_lumi.dep`

