library IEEE;
use IEEE.std_logic_1164.all;

library work;
use work.ipbus.all;
use work.bril_histogram_package.all;

entity ot_lumi_wrapper is
    port(
      -- IPbus
      ipb_clk             : in std_logic;
      ipb_rst             : in std_logic;
      ipb_in              : in ipb_wbus;  
      ipb_out             : out ipb_rbus;
  
      -- Stub
      clk                 : in  std_logic;
      rst                 : in  std_logic;
      tt                  : in  std_logic
    );
end entity ot_lumi_wrapper;

  

architecture rtl of ot_lumi_wrapper is
  -- Internal signals

  signal lumi_section           : std_logic_vector(31 downto 0) := (others => '0');
  signal lumi_nibble            : std_logic_vector(31 downto 0) := (others => '0');
  signal new_histogram          : std_logic := '0';
  signal bc0_signal             : std_logic := '0';
  signal bg0_signal             : std_logic := '0'; 
  signal bril_data              : std_logic_vector(31 downto 0) := (others => '0');
  signal bril_fifo_rd_en        : std_logic := '0'; 
  signal bril_fifo_empty        : std_logic := '0'; 
  signal bril_fifo_words_cnt    : std_logic_vector(31 downto 0) := (others => '0'); 
  signal bril_fifo_data_valid   : std_logic := '0';
  signal user_reset             : std_logic := '0';

begin
  -- Instantiate the bril_histogram_top_sync entity
  bril_histogram_top_sync_inst: entity work.bril_histogram_top_sync
    generic map (
      -- mux ratio (defines data period, can be also clock ratio)
      MUX_RATIO                               => 1, 
      -- identification of the histogram
      HISTOGRAM_TYPE                          => 0,
      HISTOGRAM_ID                            => 1,
      -- memory
      GENERATE_FIFO                           => true,
      -- histogram parameters
      NUMBER_OF_BINS_PER_CLK                  => 1, -- defines how many bins are transferred every clk_data_i, has to be N*MUX_RATIO, N >= 1
      NUMBER_OF_BINS_TOTAL                    => 5,
      COUNTER_WIDTH                           => 16,
      INCREMENT_WIDTH                         => 1,
      -- orbit and bx counter width
      ORBIT_BX_COUNTER_WIDTH                  => 16,
      -- accumulation parameters
      NUMBER_OF_UNITS                         => 1, -- number of units (modules, chips) connected to the histogrammer
      UNIT_INCREMENT_WIDTH                    => 1, -- width of the data bus per each unit
      NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE  => 4, -- defines the complexity of the accumulation algorithm
      UNIT_ERROR_COUNTER_WIDTH                => 3, -- width of the error counter    
      CHECK_ERRORS                            => false,
      STORE_ERRORS                            => false
    )
    port map (
      -- core signals
      async_reset_i       => '0', -- sync reset
      clk_data_i          => clk, -- data clock, at which all inputs (increments, new histogram/iteration, tcds) arrive, not more than 320MHz
      clk_data_en_i       => '1', -- clock enable signal (can be used when clk_data_i is same as clk_histogramming_i but MUX_RATIO is more)
      clk_histogramming_i => clk, -- histogramming clock, allows to spare fpga resources by processing multiple bins in parallel, not more than 320MHz
      -- tcds data
      lhc_fill_i          => (others => '0'),
      cms_run_i           => (others => '0'),
      lumi_section_i      => lumi_section,
      lumi_nibble_i       => lumi_nibble, 
      -- SYNC MODE - tcds signals
      data_latency_i      => (others => '0'), -- allows to delay bco/oc0 signals to correctly align the bins
      new_histogram_i     => bg0_signal, -- start outputting data (orbit reset)
      new_iteration_i     => bc0_signal, -- resetting the bin counter (if number of bins is 3564 corresponding to 3564bx - this signal is the bx reset)
      -- data input interface (used both in sync and async mode)
      increment_i         => (others => '1'),
      error_i             => (others => '0'),
      mask_i              => (others => '0'),
      -- fifo interface (optional)
      fifo_rd_clk_i       => ipb_clk,
      fifo_rd_en_i        => bril_fifo_rd_en,
      fifo_empty_o        => bril_fifo_empty, -- Error fixed: added 'open' to indicate unconnected signal
      fifo_words_cnt_o    => bril_fifo_words_cnt, -- Подключаем формальный сигнал к реальному сигналу fifo_words_cnt
      data_o              => bril_data, -- output of the memory
      data_valid_o        => bril_fifo_data_valid -- Error fixed: added 'open' to indicate unconnected signal
    );

  -- Instantiate the ot_lumi entity
  ot_lumi_inst: entity work.ot_lumi
  port map(
    ipb_clk             => ipb_clk,
    ipb_rst             => ipb_rst,
    ipb_in              => ipb_in,
    ipb_out             => ipb_out,
    clk                 => clk,
    rst                 => rst,
    tt                  => tt
  );

end architecture rtl;
