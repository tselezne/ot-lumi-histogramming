VIVADO_DIR=/home/Software/Xilinx/Vivado/2019.1

vlib glbl
vlog -work glbl $VIVADO_DIR/data/verilog/src/glbl.v

vcom -work work $(shell pwd)../../../../bril_histogram/components/bril_histogram/firmware/hdl/bril_histogram_package.vhd
vcom -work work $(shell pwd)../../../../bril_histogram/components/bril_histogram/firmware/hdl/bril_histogram_readout_ipbus.vhd
vcom -work work $(shell pwd)../../../../bril_histogram/components/bril_histogram/firmware/hdl/sync/bril_histogram_top_sync.vhd
vcom -work work $(shell pwd)../../../../bril_histogram/components/bril_histogram/firmware/hdl/sync/bril_histogram_latency_compensation_sync.vhd
vcom -work work $(shell pwd)../../../../bril_histogram/components/bril_histogram/firmware/hdl/bril_histogram_reset_synchroniser.vhd
vcom -work work $(shell pwd)../../../..//bril_histogram/components/bril_histogram/firmware/hdl/bril_histogram_error_block.vhd
vcom -work work $(shell pwd)../../../../bril_histogram/components/bril_histogram/firmware/hdl/sync/bril_histogram_demultiplexer_sync.vhd
vcom -work work $(shell pwd)../../../../bril_histogram/components/bril_histogram/firmware/hdl/sync/bril_histogram_accumulation_layer_sync.vhd
vcom -work work $(shell pwd)../../../../bril_histogram/components/bril_histogram/firmware/hdl/sync/bril_histogram_core_sync.vhd
vcom -work work $(shell pwd)../../../../bril_histogram/components/bril_histogram/firmware/hdl/sync/bril_histogram_streamer_sync.vhd
vcom -work work $(shell pwd)../../../../bril_histogram/components/bril_histogram/firmware/hdl/sync/bril_histogram_output_sync.vhd
vcom -work work $(shell pwd)../../../..//bril_histogram/components/bril_histogram/firmware/hdl/bril_histogram_recursive_accumulator.vhd

vcom -work work $(shell pwd)../../../../ipbus-firmware/components/ipbus_core/firmware/hdl/ipbus_package.vhd
vcom -work work $(shell pwd)../../../../ipbus-firmware/components/ipbus_slaves/firmware/hdl/ipbus_reg_types.vhd
vcom -work work $(shell pwd)../../../../ipbus-firmware/components/ipbus_slaves/firmware/hdl/ipbus_syncreg_v.vhd
vcom -work work $(shell pwd)../../../../ipbus-firmware/components/ipbus_slaves/firmware/hdl/syncreg_w.vhd
vcom -work work $(shell pwd)../../../../ipbus-firmware/components/ipbus_slaves/firmware/hdl/syncreg_r.vhd
vcom -work work $(shell pwd)../../../../ipbus-firmware/components/ipbus_core/firmware/hdl/ipbus_fabric_sel.vhd

vcom -work work $(shell pwd)../hdl/ttc_lumi_decoder.vhd
vcom -work work $(shell pwd)ot_lumi_wrapper.vhd

#vcom -work work $VIVADO_DIR/data/verilog/src/glbl.v