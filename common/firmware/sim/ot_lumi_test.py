import cocotb
from cocotb.clock import Clock
from cocotb.triggers import Timer, RisingEdge, FallingEdge
import pandas as pd

# Ваш код
@cocotb.test()
async def test_lumi_output_change(dut):

    dut.ot_lumi_inst.bc0_signal <= 0
    dut.ot_lumi_inst.bg0_signal <= 0
    dut.ipb_rst <= 0 
    
    clk = Clock(dut.clk, 25, units="ns") 
    cocotb.fork(clk.start())

    ipb_clk = Clock(dut.ipb_clk, 25, units="ns")  
    cocotb.fork(ipb_clk.start())

    #signals_to_save = ['bc0_signal', 'bg0_signal', 'lumi_nibble', 'lumi_section', 'bril_data']
    signals_to_save = ['async_reset_i', 'clk_data_i','clk_data_en_i','clk_histogramming_i' ,
                       'lhc_fill_i', 'cms_run_i','lumi_section_i', 'lumi_nibble_i','data_latency_i' ,
                       'new_histogram_i', 'new_iteration_i', 'increment_i', 'error_i', 'mask_i', 
                       'fifo_rd_clk_i', 'fifo_rd_en_i','fifo_empty_o', 'fifo_words_cnt_o', 
                       'data_o', 'data_valid_o']

    signal_data = []

    #initial_values = {signal: getattr(dut.ot_lumi_inst, signal).value.integer if str(getattr(dut.ot_lumi_inst, signal).value).isdigit() else str(getattr(dut.ot_lumi_inst, signal).value) for signal in signals_to_save}
    #print("Initial values:", initial_values)
    #signal_data.append(initial_values)

    async def record_signals(duration_nano_sec):
        end_time = cocotb.utils.get_sim_time('ns') + duration_nano_sec  # Преобразуйте секунды в наносекунды
        while cocotb.utils.get_sim_time('ns') < end_time:
            await RisingEdge(dut.clk)
            await RisingEdge(dut.ipb_clk)
            dut.tt.value <= 1
            #current_values = {signal: getattr(dut.ot_lumi_inst, signal).value.integer if str(getattr(dut.ot_lumi_inst, signal).value).isdigit() else str(getattr(dut.ot_lumi_inst, signal).value) for signal in signals_to_save}
            current_values = {signal: getattr(dut.bril_histogram_top_sync_inst, signal).value.integer if str(getattr(dut.bril_histogram_top_sync_inst, signal).value).isdigit() else str(getattr(dut.bril_histogram_top_sync_inst, signal).value) for signal in signals_to_save}
            signal_data.append(current_values)

    await record_signals(duration_nano_sec=76900*10)  

    df = pd.DataFrame(signal_data)

    df['time'] = [cocotb.utils.get_sim_time('ns') + i * 25 for i in range(len(df))]

    df.to_pickle("signal_data.pkl")
