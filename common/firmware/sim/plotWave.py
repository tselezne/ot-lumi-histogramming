import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_pickle("signal_data.pkl")
print(df.to_string())

plt.figure()

for col in df.columns:
    if col != 'time' and col != 'bril_data_o':
        if df[col].dtype == 'O':
            plt.plot(df['time'], [0 if isinstance(val, str) else val for val in df[col]], label=col)
        else:
            plt.plot(df['time'], df[col], label=col)

plt.title("Signals")
plt.xlabel("Time (ns)")
plt.legend()
#plt.show()
