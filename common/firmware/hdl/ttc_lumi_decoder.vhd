library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity ttc_lumi_decoder is
  port(
    clk                : in  std_logic;
    rst                : in  std_logic;
    bc0_signal_i       : in  std_logic;
    bg0_signal_i       : in  std_logic;  
    bgo_value          : in  std_logic_vector(7 downto 0);
    reset_nibble_reg   : in  std_logic;
    reset_nibble_ttc   : in  std_logic;
    --bc0_signal         : out std_logic;
    --bg0_signal         : out std_logic;
    lumi_section_o     : out std_logic_vector(31 downto 0);
    lumi_nibble_o      : out std_logic_vector(31 downto 0)
  );
end entity;

architecture Behavioural of ttc_lumi_decoder is
  signal nibble_incr_flag       : std_logic;
  signal nibble_rst_flag        : std_logic;
  signal enable_ttc_rst         : std_logic;
  signal lumi_section           : unsigned(31 downto 0);
  signal lumi_nibble            : unsigned(31 downto 0);
  --signal bg0_signal             : std_logic; -- Declare as a signal
  
begin

  lumi_nibble_o   <= std_logic_vector(lumi_nibble);
  lumi_section_o  <= std_logic_vector(lumi_section);

  process(clk)
  begin
    if rising_edge(clk) then
      if reset_nibble_reg = '1' then
        lumi_nibble  <= to_unsigned(1,32);
        lumi_section <= to_unsigned(1,32);
        nibble_incr_flag <= '0';
        nibble_rst_flag  <= '0';
        enable_ttc_rst   <= '0';
        --bc0_signal <= '0';
        --bg0_signal <= '0';
      else 
        if enable_ttc_rst = '1' and bg0_signal_i = '1' then
          nibble_rst_flag   <= '1';  
          enable_ttc_rst    <= '0';
        end if;

        if reset_nibble_ttc = '1' then 
          enable_ttc_rst <= '1';
        end if;
        
        if bg0_signal_i = '1' then
          nibble_incr_flag   <= '1';  
        end if;
        
        if bc0_signal_i = '1' then
          if nibble_rst_flag   = '1' then
            --bg0_signal <= nibble_incr_flag;
            lumi_section <= to_unsigned(1,32);
            lumi_nibble  <= to_unsigned(1,32);
            nibble_rst_flag  <= '0';
            nibble_incr_flag <= '0';
          elsif nibble_incr_flag = '1' then
            nibble_incr_flag <= '0';
         
            if lumi_nibble < to_unsigned(64,32) then
              lumi_nibble <= lumi_nibble + 1;
            else
              lumi_section <= lumi_section + 1;
              lumi_nibble <= to_unsigned(1,32);
            end if;
          end if;
        end if;

      end if;
    end if;
  end process;

end architecture;

