-- IEEE VHDL standard library:
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

-- Library work
library work;
use work.ipbus.all;
use work.ipbus_reg_types.all;
use work.ipbus_decode_ot_lumi.all;
--use work.memory_config.all;

-- bril package
use work.bril_histogram_package.all;

-- fifo
library xpm;
use xpm.vcomponents.all;
-- This is the LHC structure
--
--   (0)(1).(DEB)(0)(1).(DEB)...(0)(1).(DEB)(0)(1).(DEB)(0)(1).(DEB)...(0)(1).(DEB)...(0)(1).(DEB)(0)(1).(DEB)...(0)(1).(DEB)(0)(1)...
--   [    0     ][     1    ]...[   4095   ][   4096   ][   4097   ]...[   8191   ]...[  258048  ][  258049  ]...[  262143  ][  262144  ]...
--   {            Nibble  1                }{            Nibble  2                }...{            Nibble 64                }{            Nibble 65                }...
--   <                                                 Lumi Section  1                                                      ><          Lumi Section  2
--  ^                                      ^                                      ^  ^                                      ^
-- BGo0                                   BGo0                                 BGo0  BGo0                                  BGo0
-- OC0 
-- Start
-- BC0 every 3564 BX
--

--

--

--

--

--

--

--

--
-- (1 BX)                           =   25 ns (40    MHz),  counter 12 bits,  0-3563 (N/A @BRIL module)
-- [1 Orbit]          = 3564 BX     = 89.1 us (11.22 kHz),  counter 32 bits   0-...  (N/A @BRIL module)
-- {1 Nibble}         = 4096 orbits =  365 ms (2.74   Hz),  counter  6 bits   1-64   (32b @BRIL module)
-- <1 Lumi Section>   = 64 Nibbles  = 23.3 s  (42    mHz),  counter 12 bits   1-...  (32b @BRIL module)


entity ot_lumi is
  port(
    -- ipbus
    ipb_clk             : in std_logic := '0';
    ipb_rst             : in std_logic := '0';
    ipb_in              : in ipb_wbus; 
    ipb_out             : out ipb_rbus;

    -- ttc
    --bc0_signal_i        : in std_logic;

    -- stub
    stub                : in std_logic;
    clk40               : in std_logic;
    clk320              : in std_logic;

    rst                 : in std_logic;

    data_out            : out std_logic_vector(31 downto 0);
    data_out_valid      : out std_logic;

    --fifo
    rst_loc             : in  std_logic_vector(31 downto 0);

    --stub_header  : in std_logic_vector(63 downto 0);
    stub_payload : in std_logic_vector(63 downto 0) := (others => '0');
    stub_valid   : in std_logic;
    bc0_payload  : in std_logic;
    nibble_o     : out ipb_reg_v(0 downto 0)

  );
end entity;

architecture Behavioural of ot_lumi is

  -- IPBUS BRANCHES
  signal ipb_to_slaves  : ipb_wbus_array(N_SLAVES - 1 downto 0);
  signal ipb_from_slaves: ipb_rbus_array(N_SLAVES - 1 downto 0);
  -- IPBUS REGISTER
  constant N_STAT : integer := 4;
  constant N_CTRL : integer := 4;
  signal ctrl_stb: std_logic_vector(N_CTRL-1 downto 0);
  signal stat: ipb_reg_v(N_STAT-1 downto 0);
  signal conf: ipb_reg_v(N_CTRL-1 downto 0);


  -- BRIL Histogramming signals
  signal lumi_section                  : std_logic_vector(31 downto 0);
  signal lumi_nibble                   : std_logic_vector(31 downto 0);
  signal new_histogram                 : std_logic;
  signal bc0_signal                    : std_logic;
  signal bg0_signal                    : std_logic; 
 
  signal bril_data_40_1                : std_logic_vector(31 downto 0);
  signal bril_data_40_stub             : std_logic_vector(31 downto 0);
  signal bril_data_async               : std_logic_vector(31 downto 0);
  
  signal bril_fifo_rd_en_40_1          : std_logic; 
  signal bril_fifo_rd_en_40_stub       : std_logic;
  signal bril_fifo_rd_en_async         : std_logic;
  
  signal bril_fifo_empty_40_1          : std_logic; 
  signal bril_fifo_empty_40_stub       : std_logic; 
  signal bril_fifo_empty_async         : std_logic; 
  
  signal bril_fifo_words_cnt_40_1      : std_logic_vector(31 downto 0); 
  signal bril_fifo_words_cnt_40_stub   : std_logic_vector(31 downto 0); 
  signal bril_fifo_words_cnt_async     : std_logic_vector(31 downto 0); 
  
  signal bril_fifo_data_valid_40_1     : std_logic;
  signal bril_fifo_data_valid_40_stub  : std_logic;
  signal bril_fifo_data_valid_async    : std_logic;
  
  signal user_reset_40_1               : std_logic;
  signal user_reset_40_stub            : std_logic;
  signal user_reset_async             : std_logic;
  
  signal iteration_id_i : std_logic_vector(15 downto 0); -- 12-bit for range 0-4097
  signal bin_id_i       : std_logic_vector(15 downto 0); -- 12-bit for range 0-3564
  signal bg0_timer      : integer range 0 to 4096 := 0;  
  signal bc0_timer      : integer range 0 to 3564 := 0;  


  -- fifo related signals
  signal fifo_wr_en : std_logic := '0';
  signal fifo_rd_en : std_logic := '0';
  signal fifo_rd_rst_busy, fifo_wr_rst_busy : std_logic;
  signal fifo_full, fifo_empty, fifo_data_valid : std_logic;
  signal empty_wr_dff, empty_wr_dff2, empty_wr_re : std_logic;
  signal fifo_rd_data_count_ipb, fifo_dout : std_logic_vector( 31 downto 0 );
  signal fifo_rd_data_count : std_logic_vector( 11 downto 0 );
  signal never_full : std_logic := '1';
  signal fifo_rst_from_ipb, fifo_rst : std_logic;

  signal inter_fifo_wr_en : std_logic := '0';
  signal inter_fifo_rd_en : std_logic := '0';
  signal inter_fifo_rd_rst_busy, inter_fifo_wr_rst_busy : std_logic;
  signal inter_fifo_full, inter_fifo_empty, inter_fifo_data_valid : std_logic;
  signal inter_fifo_din, inter_fifo_dout : std_logic_vector( 7 downto 0 );
  
  --clock frequency divider 
  signal clk5 : std_logic := '0';
  signal clk40_counter : integer range 0 to 7 := 0;

  --offset sorter
    signal bx : std_logic_vector(7 downto 0) := (others => '0');
    signal read_counter : integer := 0;
    signal write_counter : integer := 0;
    signal output_ready : std_logic := '0';
    
    signal data_buffer : std_logic_vector(7 downto 0) := (others => '0');  
    signal write_enable : std_logic := '0';   
    signal data_consumed : std_logic := '0';  
    signal output_data :std_logic_vector(7 downto 0) := (others => '0');
    signal data_ready : std_logic := '0';
    
    signal temp_lword_reg : std_logic_vector(63 downto 0);
    
    type bx_array is array(0 to 7) of integer;
    signal intermediate_bx : bx_array := (others => 0);
    signal intermediate_bx_ready : std_logic := '0';
    
    signal prev_value : std_logic_vector(11 downto 0) := (others => '0');  
    signal start_count : std_logic := '0';  
    signal first_change_detected : std_logic := '0';  
    
    signal sum_result, sum_fifo_dout : std_logic_vector(15 downto 0) := (others => '0');  
    signal sum_fifo_wr_en : std_logic := '0';  
    signal sum_ready : std_logic := '0';  
    signal sum_write_enable : std_logic := '0';  
    signal sum_fifo_empty : std_logic;  
    signal sum_fifo_full, sum_fifo_rd_en : std_logic;
    signal sum_histogram_ready : std_logic := '0';  
    
    signal bx_index : integer := 0;
    signal intermediate_bx_snapshot : bx_array := (others => 0); 
    signal capture_data : std_logic := '0';  


  -------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals declaration
  signal conf_reset_nibble_reg, conf_reset_nibble_ttc : std_logic := '0';

  signal conf_bgo_value : std_logic_vector(7 downto 0) := (others => '0');
  signal conf_data_latency : std_logic_vector(15 downto 0) := (others => '0');
  
  -- End of automatically-generated VHDL code for register "breakout" signals declaration

  --------------------------------------------------------------------------------


begin
  
  --------------------------------------------------------------------------------
  -- Start of automatically-generated VHDL code for register "breakout" signals assignment

  conf_bgo_value             <= conf(16#02#)(7 downto 0);
  conf_reset_nibble_reg      <= conf(16#02#)(12);
  conf_data_latency          <= conf(16#02#)(31 downto 16);

  -- End of automatically-generated VHDL code for register "breakout" signals assignment
  --------------------------------------------------------------------------------

  --------------------------------------
  --------------------------------------
  -- IPbus
  --------------------------------------
  --------------------------------------
  
  -- ipbus address decode
  fabric: entity work.ipbus_fabric_sel
    generic map(
      NSLV => N_SLAVES,
      SEL_WIDTH => IPBUS_SEL_WIDTH)
    port map(
      ipb_in => ipb_in,
      ipb_out => ipb_out,
      sel => ipbus_sel_ot_lumi(ipb_in.ipb_addr),
      ipb_to_slaves => ipb_to_slaves,
      ipb_from_slaves => ipb_from_slaves
    );
  
  sync_reg: entity work.ipbus_syncreg_v
    generic map(N_CTRL => N_CTRL, N_STAT => N_STAT)
    port map(
      clk => ipb_clk,
      rst => ipb_rst,
      ipb_in => ipb_to_slaves(N_SLV_OT_LUMI_REGISTERS),
      ipb_out => ipb_from_slaves(N_SLV_OT_LUMI_REGISTERS),
      slv_clk => clk40,
      d => stat,
      q => conf,
      stb => open
    );



    accumulate_data : process(clk320, rst)
    begin
        if rst = '1' then
            intermediate_bx <= (others => 0);
            intermediate_bx_snapshot <= (others => 0);
            read_counter <= 0;
            capture_data <= '0';
        elsif rising_edge(clk320) then
            -- Накопление данных
            if stub_valid = '1' and read_counter < 64 then
                temp_lword_reg <= stub_payload;
    
            --if read_counter < 64 then
                case temp_lword_reg(21 downto 19) is
                    when "000" => intermediate_bx(0) <= intermediate_bx(0) + 1;
                    when "001" => intermediate_bx(1) <= intermediate_bx(1) + 1;
                    when "010" => intermediate_bx(2) <= intermediate_bx(2) + 1;
                    when "011" => intermediate_bx(3) <= intermediate_bx(3) + 1;
                    when "100" => intermediate_bx(4) <= intermediate_bx(4) + 1;
                    when "101" => intermediate_bx(5) <= intermediate_bx(5) + 1;
                    when "110" => intermediate_bx(6) <= intermediate_bx(6) + 1;
                    when "111" => intermediate_bx(7) <= intermediate_bx(7) + 1;
                    when others => null;
                end case;
            end if;

        --end if;
    
          read_counter <= read_counter + 1;
  
          if read_counter = 64 then
              intermediate_bx_snapshot <= intermediate_bx;  
              intermediate_bx <= (others => 0);  
              capture_data <= '1';  
              read_counter <= 0; 
          else
              capture_data <= '0';  
          end if;
        end if;
    end process;

    send_to_fifo : process(clk320, rst)
    variable transfer_active : boolean := false;
begin
    if rst = '1' then
        bx_index <= 0;
        inter_fifo_din <= (others => '0');
        inter_fifo_wr_en <= '0';
        transfer_active := false;
    elsif rising_edge(clk320) then
        if capture_data = '1' then  
            transfer_active := true;
            bx_index <= 0;
        end if;

        if transfer_active then
            if bx_index <= 7 then
                -- Передаем данные в FIFO
                inter_fifo_din <= std_logic_vector(to_unsigned(intermediate_bx_snapshot(bx_index), 8));
                inter_fifo_wr_en <= '1';  
                bx_index <= bx_index + 1;
            else
                -- Завершаем передачу
                inter_fifo_wr_en <= '0';  
                bx_index <= 0;  
                transfer_active := false;  
            end if;
        else
            inter_fifo_wr_en <= '0';  
        end if;
    end if;
end process;

  data_process_40 : process(clk40, rst)
begin
    if rst = '1' then
        output_data <= (others => '0');
        write_counter <= 0;
        output_ready <= '0';
        inter_fifo_rd_en <= '0';  
    elsif rising_edge(clk40) then
        if inter_fifo_empty = '0' then
            inter_fifo_rd_en <= '1';  
            output_data <= inter_fifo_dout;  
            output_ready <= '1';  
        
            write_counter <= write_counter + 1;
            if write_counter = 8 then
                write_counter <= 0;
                output_ready <= '0';  
            end if;
        else
            inter_fifo_rd_en <= '0';  
        end if;
    end if;
end process;


  bx_fifo_inst : xpm_fifo_async
      generic map (
          FIFO_MEMORY_TYPE        => "auto",    
          --SLEEP                  => '0',
          FIFO_WRITE_DEPTH        => 16,  
          WRITE_DATA_WIDTH        => 8,       
          READ_DATA_WIDTH         => 8,       
          --WRITE_MODE              => "fwft",  
          FIFO_READ_LATENCY       => 1,         
          READ_MODE               => "fwft"    
      )
      port map (
          -- write ports
          rst        => rst,
          injectsbiterr => '0',
          injectdbiterr => '0',
          wr_clk     => clk320,              
          din        => inter_fifo_din,               
          wr_en      => inter_fifo_wr_en,        
          full       => inter_fifo_full,          
          sleep      => '0',
          -- read ports
          rd_clk     => clk40,             
          dout       => inter_fifo_dout,              
          rd_en      => inter_fifo_rd_en,             
          empty      => inter_fifo_empty          
      );



  ----------------------------------------

  bc0_bg0 : process(clk40)  
  variable nibble_timer : natural range 1 to 64 := 1;  
  variable section_timer : natural range 1 to 10000 := 1; 
  begin
    if rising_edge(clk40) then
        -- BC0 Signal Generation
        if bc0_timer = 3564 then
            bc0_signal <= '1';  
            bc0_timer <= 0;  
        else
            bc0_timer <= bc0_timer + 1;
            bc0_signal <= '0';  
        end if;

        if bc0_signal = '1' then
          bg0_timer <= bg0_timer + 1;
          if bg0_timer = 4095 then --4096
            bg0_signal <= '1';  
            bg0_timer <= 0; 
          else
            bg0_signal <= '0';          
          end if;
        end if;     
      end if;
    end process bc0_bg0;
    

    ttc_decoder_inst: entity work.ttc_lumi_decoder
    port map(
      -- TTC inputs
      clk                 => clk40                      ,
      rst                 => rst                      ,
      bc0_signal_i        => bc0_signal               ,
      bg0_signal_i        => bg0_signal               ,
      -- Config
      bgo_value           => conf_bgo_value           ,
      reset_nibble_reg    => rst,--conf_reset_nibble_reg    ,
      reset_nibble_ttc    => conf_reset_nibble_ttc    ,
      -- Outputs
      --bc0_signal          => bc0_signal               ,   
      --bg0_signal          => bg0_signal               ,
      lumi_nibble_o       => lumi_nibble              ,  
      lumi_section_o      => lumi_section   
    );
  
  nibble_o(0) <= lumi_nibble; --std_logic_vector(to_unsigned(lumi_nibble, nibble_o(0)'length));

  
  bin_iter: process(clk320, rst)
  variable local_bin_id      : integer range 0 to 3564;
  variable local_iteration_id: integer range 0 to 4097 := 0;
  begin
    if rst = '1' then
      bin_id_i <= (others => '0');
      iteration_id_i <= (others => '0');
    elsif rising_edge(clk320) then
      -- Calculate bin_id
      if stub_valid = '1' then
        local_bin_id := bc0_timer;-- + to_integer(unsigned(stub_payload(21 downto 19)));

        -- If bin_id exceeds 3564, adjust it and increment iteration_id
        if local_bin_id > 3564 then
          local_bin_id := local_bin_id - 3564;
          local_iteration_id := bg0_timer + 1;
        else
          local_iteration_id := bg0_timer;
        end if;

        -- If iteration_id exceeds 4097, reset it to 1
        if local_iteration_id > 4096 then
          local_iteration_id := 1;
        end if;
  
      end if;

      -- Assign the calculated values to the outputs
      bin_id_i <= std_logic_vector(to_unsigned(local_bin_id, 16));
      iteration_id_i <= std_logic_vector(to_unsigned(local_iteration_id, 16));
    end if;
  end process bin_iter;


  --------------------------------------
  --------------------------------------
  -- BRIL MODULES
  --------------------------------------
  --------------------------------------
  
  bril_histogram_top_sync_inst_40_1: entity work.bril_histogram_top_sync
  generic map(
  -- mux ratio (defines data period, can be also clock ratio)
    MUX_RATIO                               => 1,
  -- identification of the histogram
    HISTOGRAM_TYPE                          => 1,
    HISTOGRAM_ID                            => 1,
  -- memory
    GENERATE_FIFO                           => true,
  -- histogram parameters
    NUMBER_OF_BINS_PER_CLK                  => 1, -- defines how many bins are transferred every clk_data_i, has to be N*MUX_RATIO, N >= 1
    NUMBER_OF_BINS_TOTAL                    => 3564,
    COUNTER_WIDTH                           => 16,
    INCREMENT_WIDTH                         => 1,
  -- orbit and bx counter width
    ORBIT_BX_COUNTER_WIDTH                  => 16,
  -- accumulation parameters
    NUMBER_OF_UNITS                         => 1, -- number of units (modules, chips) connected to the histogrammer
    UNIT_INCREMENT_WIDTH                    => 1, -- width of the data bus per each unit
    NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE  => 1, -- defines the complexity of the accumualtion algorithm
    UNIT_ERROR_COUNTER_WIDTH                => 3, -- width of the error counter    
    CHECK_ERRORS                            => false,
    STORE_ERRORS                            => false
  )
  port map(
  -- core signals
    async_reset_i       => user_reset_40_1, --async_reset, -- sync reset
    clk_data_i          => clk40, -- data clock, at which all inputs (increments, new histogram/iteration, tcds) arrive, not more than 320MHz
    clk_data_en_i       => '1', -- clock enable signal (can be used when clk_data_i is same as clk_histogramming_i but MUX_RATIO is more)
    clk_histogramming_i => clk40, -- histogramming clock, allows to spare fpga resources by processing multiple bins in parallel, not more than 320MHz
  -- tcds data
    lhc_fill_i          => (others => '0'),--conf_fill_number,
    cms_run_i           => (others => '0'),--conf_run_number,
    lumi_section_i      => lumi_section,
    lumi_nibble_i       => lumi_nibble,
  -- SYNC MODE - tcds signals
    data_latency_i      => conf_data_latency,
    new_histogram_i     => bg0_signal,-- bg0_signal,-- -- start outputting data (orbit reset)
    new_iteration_i     => bc0_signal, -- resetting the bin counter (if number of bins is 3564 corresponding to 3564bx - this signal is the bx reset)
    -- data input interface (used both in sync and async mode)
    increment_i(0)      => '1',
    error_i             => (others => '0'),
    mask_i              => (others => '0'),
    -- fifo interface (optional)
    fifo_rd_clk_i       => ipb_clk,
    fifo_rd_en_i        => bril_fifo_rd_en_40_1,
    fifo_empty_o        => bril_fifo_empty_40_1,
    fifo_words_cnt_o    => bril_fifo_words_cnt_40_1,
    --
    data_o              => bril_data_40_1, -- output of the memory
    data_valid_o        => bril_fifo_data_valid_40_1 -- write enable
  );
 
  
  bril_histogram_top_sync_inst_40_stub: entity work.bril_histogram_top_sync
  generic map(
  -- mux ratio (defines data period, can be also clock ratio)
    MUX_RATIO                               => 1,
  -- identification of the histogram
    HISTOGRAM_TYPE                          => 1,
    HISTOGRAM_ID                            => 1,
  -- memory
    GENERATE_FIFO                           => true,
  -- histogram parameters
    NUMBER_OF_BINS_PER_CLK                  => 1, -- defines how many bins are transferred every clk_data_i, has to be N*MUX_RATIO, N >= 1
    NUMBER_OF_BINS_TOTAL                    => 3564, --3564
    COUNTER_WIDTH                           => 16,
    INCREMENT_WIDTH                         => 8,
  -- orbit and bx counter width
    ORBIT_BX_COUNTER_WIDTH                  => 16,
  -- accumulation parameters
    NUMBER_OF_UNITS                         => 1, -- number of units (modules, chips) connected to the histogrammer
    UNIT_INCREMENT_WIDTH                    => 8, -- width of the data bus per each unit
    NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE  => 1, -- defines the complexity of the accumualtion algorithm
    UNIT_ERROR_COUNTER_WIDTH                => 1, -- width of the error counter    
    CHECK_ERRORS                            => false,
    STORE_ERRORS                            => false
  )
  port map(
  -- core signals
    async_reset_i       => user_reset_40_stub, --async_reset, -- sync reset
    clk_data_i          => clk40, -- data clock, at which all inputs (increments, new histogram/iteration, tcds) arrive, not more than 320MHz
    clk_data_en_i       => '1', -- clock enable signal (can be used when clk_data_i is same as clk_histogramming_i but MUX_RATIO is more)
    clk_histogramming_i => clk40, -- histogramming clock, allows to spare fpga resources by processing multiple bins in parallel, not more than 320MHz
  -- tcds data
    lhc_fill_i          => (others => '0'),--conf_fill_number,
    cms_run_i           => (others => '0'),--conf_run_number,
    lumi_section_i      => lumi_section,
    lumi_nibble_i       => lumi_nibble,
  -- SYNC MODE - tcds signals
    data_latency_i      => conf_data_latency,
    new_histogram_i     => bg0_signal,-- bg0_signal,-- -- start outputting data (orbit reset)
    new_iteration_i     => bc0_signal, -- resetting the bin counter (if number of bins is 3564 corresponding to 3564bx - this signal is the bx reset)
    -- data input interface (used both in sync and async mode)
    increment_i         => inter_fifo_dout, --stub,--(31 downto 1 => '0') & stub,
    error_i             => (others => '0'),
    mask_i              => (others => '0'),
    -- fifo interface (optional)
    fifo_rd_clk_i       => ipb_clk,
    fifo_rd_en_i        => bril_fifo_rd_en_40_stub,
    fifo_empty_o        => bril_fifo_empty_40_stub,
    fifo_words_cnt_o    => bril_fifo_words_cnt_40_stub,
    --
    data_o              => bril_data_40_stub, -- output of the memory
    data_valid_o        => bril_fifo_data_valid_40_stub -- write enable
  );
  
 
  async_histogramming_inst : entity work.bril_histogram_top_async
  generic map(
      HISTOGRAM_TYPE                         => 0,
      HISTOGRAM_ID                           => 0,
      USE_EXTERNAL_NEW_HISTOGRAM             => TRUE,
      NUMBER_OF_BINS                         => 10,
      COUNTER_WIDTH                          => 16,
      INCREMENT_WIDTH                        => 1,
      ORBIT_BX_COUNTER_WIDTH                 => 16,
      NUMBER_OF_UNITS                        => 1,
      UNIT_INCREMENT_WIDTH                   => 1,
      NUMBER_OF_UNITS_PER_ACCUMULATION_STAGE => 1,
      UNIT_ERROR_COUNTER_WIDTH               => 3,
      CHECK_ERRORS                           => TRUE,
      STORE_ERRORS                           => TRUE
  )
  port map(
      async_reset_i       => user_reset_async,
      clk_i               => clk40,
      lhc_fill_i          => std_logic_vector(to_unsigned(0,32)),
      cms_run_i           => std_logic_vector(to_unsigned(0,32)),
      lumi_section_i      => lumi_section, --std_logic_vector(to_unsigned(0,32)),
      lumi_nibble_i       => lumi_nibble, --std_logic_vector(to_unsigned(0,32)),
      --iteration_counter_i => iteration_id,
      new_histogram_ext_i => bg0_signal,
      valid_i             => '1', --stub, --clustering_data_valid,
      iteration_id_i      => lumi_nibble(15 downto 0), -- std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0)
      iteration_counter_i => lumi_nibble(15 downto 0),--std_logic_vector(to_unsigned(0, 16)),
      bin_id_i            => sum_fifo_dout, --std_logic_vector(ORBIT_BX_COUNTER_WIDTH-1 downto 0)
      increment_i(0)      => '1', --std_logic_vector(NUMBER_OF_UNITS*UNIT_INCREMENT_WIDTH-1 downto 0)
      error_i             => std_logic_vector(to_unsigned(0,1)),
      mask_i              => std_logic_vector(to_unsigned(0,1)),
      fifo_rd_clk_i       => ipb_clk,
      fifo_rd_en_i        => bril_fifo_rd_en_async,
      fifo_empty_o        => bril_fifo_empty_async,
      fifo_words_cnt_o    => bril_fifo_words_cnt_async,
      fifo_data_o         => bril_data_async,
      fifo_data_valid_o   => bril_fifo_data_valid_async,
      init_done_o         => open--histogramming_fifo_init_done
  );


  data_out <= bril_data_40_stub;
  data_out_valid <= bril_fifo_data_valid_40_stub;

  
  readout_regs_inst_40_1: entity work.bril_histogram_readout_ipbus
  port map(
    clk                   => ipb_clk, -- ipbus clock                            
    reset                 => ipb_rst, --user_reset, --ipb_rst, -- reset in the ipbus clock domain                 
    ipbus_in              => ipb_to_slaves(N_SLV_BRIL_40_1),  -- ipbus input (SLV_BRIL_HISTOGRAM_READOUT is the slave id)
    ipbus_out             => ipb_from_slaves(N_SLV_BRIL_40_1),  -- ipbus output (SLV_BRIL_HISTOGRAM_READOUT is the slave id)
    -- fifo status
    fifo_empty_i          => bril_fifo_empty_40_1, -- connect to the respective bril histogram port
    fifo_data_valid_i     => bril_fifo_data_valid_40_1, -- connect to the respective bril histogram port
    fifo_words_cnt_i      => bril_fifo_words_cnt_40_1, -- connect to the respective bril histogram port
    -- fifo read interface
    fifo_rd_en_o          => bril_fifo_rd_en_40_1, -- connect to the respective bril histogram port
    fifo_data_i           => bril_data_40_1, -- connect to the respective bril histogram port
    -- user reset
    user_reset_o          => user_reset_40_1
  );
 

  readout_regs_inst_40_stub: entity work.bril_histogram_readout_ipbus
  port map(
    clk                   => ipb_clk, -- ipbus clock                            
    reset                 => ipb_rst, --user_reset, --ipb_rst, -- reset in the ipbus clock domain                 
    ipbus_in              => ipb_to_slaves(N_SLV_BRIL_40_stub),  -- ipbus input (SLV_BRIL_HISTOGRAM_READOUT is the slave id)
    ipbus_out             => ipb_from_slaves(N_SLV_BRIL_40_stub),  -- ipbus output (SLV_BRIL_HISTOGRAM_READOUT is the slave id)
    -- fifo status
    fifo_empty_i          => bril_fifo_empty_40_stub, -- connect to the respective bril histogram port
    fifo_data_valid_i     => bril_fifo_data_valid_40_stub, -- connect to the respective bril histogram port
    fifo_words_cnt_i      => bril_fifo_words_cnt_40_stub, -- connect to the respective bril histogram port
    -- fifo read interface
    fifo_rd_en_o          => bril_fifo_rd_en_40_stub, -- connect to the respective bril histogram port
    fifo_data_i           => bril_data_40_stub, -- connect to the respective bril histogram port
    -- user reset
    user_reset_o          => user_reset_40_stub
  );
  
  --readout_regs_inst_async: entity work.bril_histogram_readout_ipbus
  --port map(
  --  clk                   => ipb_clk, -- ipbus clock                            
  --  reset                 => ipb_rst, --user_reset, --ipb_rst, -- reset in the ipbus clock domain                 
  --  ipbus_in              => ipb_to_slaves(N_SLV_BRIL_ASYNC),  -- ipbus input (SLV_BRIL_HISTOGRAM_READOUT is the slave id)
  --  ipbus_out             => ipb_from_slaves(N_SLV_BRIL_ASYNC),  -- ipbus output (SLV_BRIL_HISTOGRAM_READOUT is the slave id)
    -- fifo status
  --  fifo_empty_i          => bril_fifo_empty_async, -- connect to the respective bril histogram port
  --  fifo_data_valid_i     => bril_fifo_data_valid_async, -- connect to the respective bril histogram port
  --  fifo_words_cnt_i      => bril_fifo_words_cnt_async, -- connect to the respective bril histogram port
    -- fifo read interface
  --  fifo_rd_en_o          => bril_fifo_rd_en_async, -- connect to the respective bril histogram port
  --  fifo_data_i           => bril_data_async, -- connect to the respective bril histogram port
    -- user reset
  --  user_reset_o          => user_reset_async
  --);  
  
  --raw_fifo : entity work.fifo_featured
  --     generic map ( FIFO_DEPTH => 2048, FIFO_WIDTH => 32, USE_DBG_CNT => false )
  --     port map (
  --        ipb_clk => ipb_clk,
  --        ipb_rst => ipb_rst,
  --        ipbus_in => ipb_to_slaves( N_SLV_FIFO ),
  --        ipbus_out => ipb_from_slaves( N_SLV_FIFO ),
  --        wr_clk => clk320,
  --       fifo_wr_en_i => '1',--
  --        --fifo_din => (31 downto 1 => '0') & stubs(1).valid,
  --        fifo_din => (31 downto 8 => '0') & bx, --CICHeader_to_std_logic_vector(header_array(1)),
  --        --fifo_din => lword_to_std_logic_vector(stubs(1)),
  --        fifo_rst_i => rst,
  --        user_reg1_o => open,
  --        user_reg2_o => open,
  --        fsm_rst => std_logic'('0')
  --     );
  

end architecture;


